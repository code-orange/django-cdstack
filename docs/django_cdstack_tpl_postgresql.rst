django\_cdstack\_tpl\_postgresql package
========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_postgresql.django_cdstack_tpl_postgresql

Submodules
----------

django\_cdstack\_tpl\_postgresql.setup module
---------------------------------------------

.. automodule:: django_cdstack_tpl_postgresql.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_postgresql
   :members:
   :undoc-members:
   :show-inheritance:
