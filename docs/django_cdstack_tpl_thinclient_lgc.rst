django\_cdstack\_tpl\_thinclient\_lgc package
=============================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_thinclient_lgc.django_cdstack_tpl_thinclient_lgc

Submodules
----------

django\_cdstack\_tpl\_thinclient\_lgc.setup module
--------------------------------------------------

.. automodule:: django_cdstack_tpl_thinclient_lgc.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_thinclient_lgc
   :members:
   :undoc-members:
   :show-inheritance:
