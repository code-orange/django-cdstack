django\_cdstack\_tpl\_cloudrouter\_v2 package
=============================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_cloudrouter_v2.django_cdstack_tpl_cloudrouter_v2

Submodules
----------

django\_cdstack\_tpl\_cloudrouter\_v2.setup module
--------------------------------------------------

.. automodule:: django_cdstack_tpl_cloudrouter_v2.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_cloudrouter_v2
   :members:
   :undoc-members:
   :show-inheritance:
