django\_cdstack\_tpl\_cloudrouter.django\_cdstack\_tpl\_cloudrouter package
===========================================================================

Submodules
----------

django\_cdstack\_tpl\_cloudrouter.django\_cdstack\_tpl\_cloudrouter.views module
--------------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_cloudrouter.django_cdstack_tpl_cloudrouter.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_cloudrouter.django_cdstack_tpl_cloudrouter
   :members:
   :undoc-members:
   :show-inheritance:
