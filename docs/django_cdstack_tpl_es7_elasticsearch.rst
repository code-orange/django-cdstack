django\_cdstack\_tpl\_es7\_elasticsearch package
================================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_es7_elasticsearch.django_cdstack_tpl_es7_elasticsearch

Submodules
----------

django\_cdstack\_tpl\_es7\_elasticsearch.setup module
-----------------------------------------------------

.. automodule:: django_cdstack_tpl_es7_elasticsearch.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_es7_elasticsearch
   :members:
   :undoc-members:
   :show-inheritance:
