django\_cdstack\_tpl\_intrt package
===================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_intrt.django_cdstack_tpl_intrt

Submodules
----------

django\_cdstack\_tpl\_intrt.setup module
----------------------------------------

.. automodule:: django_cdstack_tpl_intrt.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_intrt
   :members:
   :undoc-members:
   :show-inheritance:
