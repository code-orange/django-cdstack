django\_cdstack\_tpl\_ipcop package
===================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop

Submodules
----------

django\_cdstack\_tpl\_ipcop.setup module
----------------------------------------

.. automodule:: django_cdstack_tpl_ipcop.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_ipcop
   :members:
   :undoc-members:
   :show-inheritance:
