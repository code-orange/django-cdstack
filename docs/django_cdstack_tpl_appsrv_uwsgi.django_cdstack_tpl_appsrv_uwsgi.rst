django\_cdstack\_tpl\_appsrv\_uwsgi.django\_cdstack\_tpl\_appsrv\_uwsgi package
===============================================================================

Submodules
----------

django\_cdstack\_tpl\_appsrv\_uwsgi.django\_cdstack\_tpl\_appsrv\_uwsgi.views module
------------------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_appsrv_uwsgi.django_cdstack_tpl_appsrv_uwsgi.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_appsrv_uwsgi.django_cdstack_tpl_appsrv_uwsgi
   :members:
   :undoc-members:
   :show-inheritance:
