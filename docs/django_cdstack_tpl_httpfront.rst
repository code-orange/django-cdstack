django\_cdstack\_tpl\_httpfront package
=======================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_httpfront.django_cdstack_tpl_httpfront

Submodules
----------

django\_cdstack\_tpl\_httpfront.setup module
--------------------------------------------

.. automodule:: django_cdstack_tpl_httpfront.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_httpfront
   :members:
   :undoc-members:
   :show-inheritance:
