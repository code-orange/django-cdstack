django\_mdat\_customer package
==============================

Subpackages
-----------

.. toctree::

   django_mdat_customer.django_mdat_customer

Submodules
----------

django\_mdat\_customer.setup module
-----------------------------------

.. automodule:: django_mdat_customer.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_mdat_customer
   :members:
   :undoc-members:
   :show-inheritance:
