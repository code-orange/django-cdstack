django\_simple\_notifier.django\_simple\_notifier.migrations package
====================================================================

Submodules
----------

django\_simple\_notifier.django\_simple\_notifier.migrations.0001\_initial module
---------------------------------------------------------------------------------

.. automodule:: django_simple_notifier.django_simple_notifier.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_simple_notifier.django_simple_notifier.migrations
   :members:
   :undoc-members:
   :show-inheritance:
