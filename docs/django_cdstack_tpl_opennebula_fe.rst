django\_cdstack\_tpl\_opennebula\_fe package
============================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_opennebula_fe.django_cdstack_tpl_opennebula_fe

Submodules
----------

django\_cdstack\_tpl\_opennebula\_fe.setup module
-------------------------------------------------

.. automodule:: django_cdstack_tpl_opennebula_fe.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_opennebula_fe
   :members:
   :undoc-members:
   :show-inheritance:
