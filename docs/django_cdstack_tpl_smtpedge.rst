django\_cdstack\_tpl\_smtpedge package
======================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_smtpedge.django_cdstack_tpl_smtpedge

Submodules
----------

django\_cdstack\_tpl\_smtpedge.setup module
-------------------------------------------

.. automodule:: django_cdstack_tpl_smtpedge.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_smtpedge
   :members:
   :undoc-members:
   :show-inheritance:
