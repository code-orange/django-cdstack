django\_cdstack\_tpl\_thinclient package
========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_thinclient.django_cdstack_tpl_thinclient

Submodules
----------

django\_cdstack\_tpl\_thinclient.setup module
---------------------------------------------

.. automodule:: django_cdstack_tpl_thinclient.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_thinclient
   :members:
   :undoc-members:
   :show-inheritance:
