django\_cdstack\_tpl\_deb\_stretch package
==========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch

Submodules
----------

django\_cdstack\_tpl\_deb\_stretch.setup module
-----------------------------------------------

.. automodule:: django_cdstack_tpl_deb_stretch.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_deb_stretch
   :members:
   :undoc-members:
   :show-inheritance:
