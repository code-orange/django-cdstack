django\_cdstack\_tpl\_scm package
=================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_scm.django_cdstack_tpl_scm

Submodules
----------

django\_cdstack\_tpl\_scm.setup module
--------------------------------------

.. automodule:: django_cdstack_tpl_scm.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_scm
   :members:
   :undoc-members:
   :show-inheritance:
