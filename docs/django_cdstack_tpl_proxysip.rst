django\_cdstack\_tpl\_proxysip package
======================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_proxysip.django_cdstack_tpl_proxysip

Submodules
----------

django\_cdstack\_tpl\_proxysip.setup module
-------------------------------------------

.. automodule:: django_cdstack_tpl_proxysip.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_proxysip
   :members:
   :undoc-members:
   :show-inheritance:
