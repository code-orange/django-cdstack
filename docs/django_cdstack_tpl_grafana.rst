django\_cdstack\_tpl\_grafana package
=====================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_grafana.django_cdstack_tpl_grafana

Submodules
----------

django\_cdstack\_tpl\_grafana.setup module
------------------------------------------

.. automodule:: django_cdstack_tpl_grafana.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_grafana
   :members:
   :undoc-members:
   :show-inheritance:
