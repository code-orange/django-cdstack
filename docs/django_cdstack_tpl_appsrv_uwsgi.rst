django\_cdstack\_tpl\_appsrv\_uwsgi package
===========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_appsrv_uwsgi.django_cdstack_tpl_appsrv_uwsgi

Submodules
----------

django\_cdstack\_tpl\_appsrv\_uwsgi.setup module
------------------------------------------------

.. automodule:: django_cdstack_tpl_appsrv_uwsgi.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_appsrv_uwsgi
   :members:
   :undoc-members:
   :show-inheritance:
