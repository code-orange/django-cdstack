django\_cdstack\_tpl\_saltstack package
=======================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_saltstack.django_cdstack_tpl_saltstack

Submodules
----------

django\_cdstack\_tpl\_saltstack.setup module
--------------------------------------------

.. automodule:: django_cdstack_tpl_saltstack.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_saltstack
   :members:
   :undoc-members:
   :show-inheritance:
