django\_cdstack\_tpl\_mariadb package
=====================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_mariadb.django_cdstack_tpl_mariadb

Submodules
----------

django\_cdstack\_tpl\_mariadb.setup module
------------------------------------------

.. automodule:: django_cdstack_tpl_mariadb.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_mariadb
   :members:
   :undoc-members:
   :show-inheritance:
