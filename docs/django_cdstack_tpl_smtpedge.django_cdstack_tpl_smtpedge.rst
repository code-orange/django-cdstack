django\_cdstack\_tpl\_smtpedge.django\_cdstack\_tpl\_smtpedge package
=====================================================================

Submodules
----------

django\_cdstack\_tpl\_smtpedge.django\_cdstack\_tpl\_smtpedge.views module
--------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_smtpedge.django_cdstack_tpl_smtpedge.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_smtpedge.django_cdstack_tpl_smtpedge
   :members:
   :undoc-members:
   :show-inheritance:
