django\_cdstack\_tpl\_callswitch package
========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_callswitch.django_cdstack_tpl_callswitch

Submodules
----------

django\_cdstack\_tpl\_callswitch.setup module
---------------------------------------------

.. automodule:: django_cdstack_tpl_callswitch.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_callswitch
   :members:
   :undoc-members:
   :show-inheritance:
