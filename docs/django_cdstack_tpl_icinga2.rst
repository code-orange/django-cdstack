django\_cdstack\_tpl\_icinga2 package
=====================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_icinga2.django_cdstack_tpl_icinga2

Submodules
----------

django\_cdstack\_tpl\_icinga2.setup module
------------------------------------------

.. automodule:: django_cdstack_tpl_icinga2.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_icinga2
   :members:
   :undoc-members:
   :show-inheritance:
