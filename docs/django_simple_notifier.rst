django\_simple\_notifier package
================================

Subpackages
-----------

.. toctree::

   django_simple_notifier.django_simple_notifier

Submodules
----------

django\_simple\_notifier.setup module
-------------------------------------

.. automodule:: django_simple_notifier.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_simple_notifier
   :members:
   :undoc-members:
   :show-inheritance:
