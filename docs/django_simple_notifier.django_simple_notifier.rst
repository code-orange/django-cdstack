django\_simple\_notifier.django\_simple\_notifier package
=========================================================

Subpackages
-----------

.. toctree::

   django_simple_notifier.django_simple_notifier.migrations

Submodules
----------

django\_simple\_notifier.django\_simple\_notifier.admin module
--------------------------------------------------------------

.. automodule:: django_simple_notifier.django_simple_notifier.admin
   :members:
   :undoc-members:
   :show-inheritance:

django\_simple\_notifier.django\_simple\_notifier.models module
---------------------------------------------------------------

.. automodule:: django_simple_notifier.django_simple_notifier.models
   :members:
   :undoc-members:
   :show-inheritance:

django\_simple\_notifier.django\_simple\_notifier.plugin\_zammad module
-----------------------------------------------------------------------

.. automodule:: django_simple_notifier.django_simple_notifier.plugin_zammad
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_simple_notifier.django_simple_notifier
   :members:
   :undoc-members:
   :show-inheritance:
