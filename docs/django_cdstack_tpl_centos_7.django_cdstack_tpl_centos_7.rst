django\_cdstack\_tpl\_centos\_7.django\_cdstack\_tpl\_centos\_7 package
=======================================================================

Submodules
----------

django\_cdstack\_tpl\_centos\_7.django\_cdstack\_tpl\_centos\_7.views module
----------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_centos_7.django_cdstack_tpl_centos_7.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_centos_7.django_cdstack_tpl_centos_7
   :members:
   :undoc-members:
   :show-inheritance:
