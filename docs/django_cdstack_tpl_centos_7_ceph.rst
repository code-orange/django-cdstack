django\_cdstack\_tpl\_centos\_7\_ceph package
=============================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_centos_7_ceph.django_cdstack_tpl_centos_7_ceph

Submodules
----------

django\_cdstack\_tpl\_centos\_7\_ceph.setup module
--------------------------------------------------

.. automodule:: django_cdstack_tpl_centos_7_ceph.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_centos_7_ceph
   :members:
   :undoc-members:
   :show-inheritance:
