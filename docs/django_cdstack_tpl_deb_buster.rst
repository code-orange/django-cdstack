django\_cdstack\_tpl\_deb\_buster package
=========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster

Submodules
----------

django\_cdstack\_tpl\_deb\_buster.setup module
----------------------------------------------

.. automodule:: django_cdstack_tpl_deb_buster.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_deb_buster
   :members:
   :undoc-members:
   :show-inheritance:
