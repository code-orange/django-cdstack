django\_cdstack\_tpl\_mediasw package
=====================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_mediasw.django_cdstack_tpl_mediasw

Submodules
----------

django\_cdstack\_tpl\_mediasw.setup module
------------------------------------------

.. automodule:: django_cdstack_tpl_mediasw.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_mediasw
   :members:
   :undoc-members:
   :show-inheritance:
