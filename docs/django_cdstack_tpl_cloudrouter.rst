django\_cdstack\_tpl\_cloudrouter package
=========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_cloudrouter.django_cdstack_tpl_cloudrouter

Submodules
----------

django\_cdstack\_tpl\_cloudrouter.setup module
----------------------------------------------

.. automodule:: django_cdstack_tpl_cloudrouter.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_cloudrouter
   :members:
   :undoc-members:
   :show-inheritance:
