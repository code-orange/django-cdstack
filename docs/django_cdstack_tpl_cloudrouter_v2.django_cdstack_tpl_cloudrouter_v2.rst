django\_cdstack\_tpl\_cloudrouter\_v2.django\_cdstack\_tpl\_cloudrouter\_v2 package
===================================================================================

Submodules
----------

django\_cdstack\_tpl\_cloudrouter\_v2.django\_cdstack\_tpl\_cloudrouter\_v2.views module
----------------------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_cloudrouter_v2.django_cdstack_tpl_cloudrouter_v2.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_cloudrouter_v2.django_cdstack_tpl_cloudrouter_v2
   :members:
   :undoc-members:
   :show-inheritance:
