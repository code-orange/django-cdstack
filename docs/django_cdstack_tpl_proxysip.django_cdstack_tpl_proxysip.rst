django\_cdstack\_tpl\_proxysip.django\_cdstack\_tpl\_proxysip package
=====================================================================

Submodules
----------

django\_cdstack\_tpl\_proxysip.django\_cdstack\_tpl\_proxysip.views module
--------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_proxysip.django_cdstack_tpl_proxysip.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_proxysip.django_cdstack_tpl_proxysip
   :members:
   :undoc-members:
   :show-inheritance:
