django\_cdstack\_tpl\_faxmail.django\_cdstack\_tpl\_faxmail package
===================================================================

Submodules
----------

django\_cdstack\_tpl\_faxmail.django\_cdstack\_tpl\_faxmail.views module
------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_faxmail.django_cdstack_tpl_faxmail.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_faxmail.django_cdstack_tpl_faxmail
   :members:
   :undoc-members:
   :show-inheritance:
