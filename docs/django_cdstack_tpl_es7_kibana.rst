django\_cdstack\_tpl\_es7\_kibana package
=========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_es7_kibana.django_cdstack_tpl_es7_kibana

Submodules
----------

django\_cdstack\_tpl\_es7\_kibana.setup module
----------------------------------------------

.. automodule:: django_cdstack_tpl_es7_kibana.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_es7_kibana
   :members:
   :undoc-members:
   :show-inheritance:
