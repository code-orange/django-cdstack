django\_cdstack\_models.django\_cdstack\_models.migrations package
==================================================================

Submodules
----------

django\_cdstack\_models.django\_cdstack\_models.migrations.0001\_initial module
-------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0002\_link\_instances\_with\_users module
----------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0002_link_instances_with_users
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0003\_add\_instance\_created\_date\_to\_instances module
-------------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0003_add_instance_created_date_to_instances
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0004\_add\_created\_date\_to\_hosts module
-----------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0004_add_created_date_to_hosts
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0005\_add\_created\_date\_to\_groups module
------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0005_add_created_date_to_groups
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0006\_add\_created\_date\_to\_childgroup\_link module
----------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0006_add_created_date_to_childgroup_link
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0007\_add\_last\_fetch\_date\_to\_hosts module
---------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0007_add_last_fetch_date_to_hosts
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0008\_fix\_group\_vars\_key module
---------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0008_fix_group_vars_key
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0009\_add\_geolocation\_to\_all\_hosts module
--------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0009_add_geolocation_to_all_hosts
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0010\_add\_name\_description\_and\_admin\_comment module
-------------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0010_add_name_description_and_admin_comment
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0011\_add\_meta\_data\_tables module
-----------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0011_add_meta_data_tables
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0012\_add\_vars\_to\_instance module
-----------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0012_add_vars_to_instance
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0013\_allow\_cascading\_deletes module
-------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0013_allow_cascading_deletes
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0014\_add\_notifier\_for\_email module
-------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0014_add_notifier_for_email
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0015\_add\_operating\_system\_models module
------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0015_add_operating_system_models
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0016\_add\_operating\_system\_to\_hosts module
---------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0016_add_operating_system_to_hosts
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0017\_add\_global\_default\_vars module
--------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0017_add_global_default_vars
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0018\_add\_check\_up\_down\_and\_notify\_enabled module
------------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0018_add_check_up_down_and_notify_enabled
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0019\_add\_mon\_skip\_cmdb\_prefix\_and\_monitoring\_id module
-------------------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0019_add_mon_skip_cmdb_prefix_and_monitoring_id
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0020\_add\_app\_and\_service\_data module
----------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0020_add_app_and_service_data
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0021\_add\_agent\_based\_flag\_to\_os module
-------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0021_add_agent_based_flag_to_os
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0022\_remove\_old\_api\_credentials module
-----------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0022_remove_old_api_credentials
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0023\_add\_simple\_host\_dependencies module
-------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0023_add_simple_host_dependencies
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0024\_add\_locations\_including\_host\_dependencies module
---------------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0024_add_locations_including_host_dependencies
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0025\_add\_option\_to\_enable\_or\_disable\_monitoring module
------------------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0025_add_option_to_enable_or_disable_monitoring
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0026\_allow\_blank\_for\_location\_master\_just\_like\_model module
------------------------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0026_allow_blank_for_location_master_just_like_model
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0027\_add\_package\_update\_switch module
----------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0027_add_package_update_switch
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0028\_add\_backup\_switch module
-------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0028_add_backup_switch
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0029\_add\_cmdb\_config\_sync\_switch module
-------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0029_add_cmdb_config_sync_switch
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0030\_add\_cmdb\_sync\_reboot\_enable\_switch module
---------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0030_add_cmdb_sync_reboot_enable_switch
   :members:
   :undoc-members:
   :show-inheritance:

django\_cdstack\_models.django\_cdstack\_models.migrations.0031\_add\_last\_problem\_and\_ticketid\_fields module
-----------------------------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations.0031_add_last_problem_and_ticketid_fields
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_models.django_cdstack_models.migrations
   :members:
   :undoc-members:
   :show-inheritance:
