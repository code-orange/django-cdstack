django\_cdstack\_tpl\_cloudpbx.django\_cdstack\_tpl\_cloudpbx package
=====================================================================

Submodules
----------

django\_cdstack\_tpl\_cloudpbx.django\_cdstack\_tpl\_cloudpbx.views module
--------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_cloudpbx.django_cdstack_tpl_cloudpbx.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_cloudpbx.django_cdstack_tpl_cloudpbx
   :members:
   :undoc-members:
   :show-inheritance:
