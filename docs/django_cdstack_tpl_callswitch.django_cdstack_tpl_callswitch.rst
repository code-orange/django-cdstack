django\_cdstack\_tpl\_callswitch.django\_cdstack\_tpl\_callswitch package
=========================================================================

Submodules
----------

django\_cdstack\_tpl\_callswitch.django\_cdstack\_tpl\_callswitch.views module
------------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_callswitch.django_cdstack_tpl_callswitch.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_callswitch.django_cdstack_tpl_callswitch
   :members:
   :undoc-members:
   :show-inheritance:
