django\_cdstack\_tpl\_proxmox package
=====================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_proxmox.django_cdstack_tpl_proxmox

Submodules
----------

django\_cdstack\_tpl\_proxmox.setup module
------------------------------------------

.. automodule:: django_cdstack_tpl_proxmox.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_proxmox
   :members:
   :undoc-members:
   :show-inheritance:
