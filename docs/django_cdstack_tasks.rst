django\_cdstack\_tasks package
==============================

Subpackages
-----------

.. toctree::

   django_cdstack_tasks.django_cdstack_tasks

Submodules
----------

django\_cdstack\_tasks.setup module
-----------------------------------

.. automodule:: django_cdstack_tasks.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tasks
   :members:
   :undoc-members:
   :show-inheritance:
