django\_cdstack\_tpl\_zammad package
====================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_zammad.django_cdstack_tpl_zammad

Submodules
----------

django\_cdstack\_tpl\_zammad.setup module
-----------------------------------------

.. automodule:: django_cdstack_tpl_zammad.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_zammad
   :members:
   :undoc-members:
   :show-inheritance:
