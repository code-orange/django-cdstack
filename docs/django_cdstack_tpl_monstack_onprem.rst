django\_cdstack\_tpl\_monstack\_onprem package
==============================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_monstack_onprem.django_cdstack_tpl_monstack_onprem

Submodules
----------

django\_cdstack\_tpl\_monstack\_onprem.setup module
---------------------------------------------------

.. automodule:: django_cdstack_tpl_monstack_onprem.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_monstack_onprem
   :members:
   :undoc-members:
   :show-inheritance:
