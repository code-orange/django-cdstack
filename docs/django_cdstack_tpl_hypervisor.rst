django\_cdstack\_tpl\_hypervisor package
========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_hypervisor.django_cdstack_tpl_hypervisor

Submodules
----------

django\_cdstack\_tpl\_hypervisor.setup module
---------------------------------------------

.. automodule:: django_cdstack_tpl_hypervisor.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_hypervisor
   :members:
   :undoc-members:
   :show-inheritance:
