django\_cdstack\_tpl\_emailmb package
=====================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_emailmb.django_cdstack_tpl_emailmb

Submodules
----------

django\_cdstack\_tpl\_emailmb.setup module
------------------------------------------

.. automodule:: django_cdstack_tpl_emailmb.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_emailmb
   :members:
   :undoc-members:
   :show-inheritance:
