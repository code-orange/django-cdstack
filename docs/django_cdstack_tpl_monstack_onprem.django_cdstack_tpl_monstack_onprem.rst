django\_cdstack\_tpl\_monstack\_onprem.django\_cdstack\_tpl\_monstack\_onprem package
=====================================================================================

Submodules
----------

django\_cdstack\_tpl\_monstack\_onprem.django\_cdstack\_tpl\_monstack\_onprem.views module
------------------------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_monstack_onprem.django_cdstack_tpl_monstack_onprem.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_monstack_onprem.django_cdstack_tpl_monstack_onprem
   :members:
   :undoc-members:
   :show-inheritance:
