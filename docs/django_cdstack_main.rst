django\_cdstack\_main package
=============================

Subpackages
-----------

.. toctree::

   django_cdstack_main.django_cdstack_main

Submodules
----------

django\_cdstack\_main.setup module
----------------------------------

.. automodule:: django_cdstack_main.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_main
   :members:
   :undoc-members:
   :show-inheritance:
