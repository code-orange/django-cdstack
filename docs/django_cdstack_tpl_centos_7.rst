django\_cdstack\_tpl\_centos\_7 package
=======================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_centos_7.django_cdstack_tpl_centos_7

Submodules
----------

django\_cdstack\_tpl\_centos\_7.setup module
--------------------------------------------

.. automodule:: django_cdstack_tpl_centos_7.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_centos_7
   :members:
   :undoc-members:
   :show-inheritance:
