django\_cdstack\_tpl\_buildserver package
=========================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_buildserver.django_cdstack_tpl_buildserver

Submodules
----------

django\_cdstack\_tpl\_buildserver.setup module
----------------------------------------------

.. automodule:: django_cdstack_tpl_buildserver.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_buildserver
   :members:
   :undoc-members:
   :show-inheritance:
