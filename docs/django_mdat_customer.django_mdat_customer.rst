django\_mdat\_customer.django\_mdat\_customer package
=====================================================

Subpackages
-----------

.. toctree::

   django_mdat_customer.django_mdat_customer.migrations

Submodules
----------

django\_mdat\_customer.django\_mdat\_customer.admin module
----------------------------------------------------------

.. automodule:: django_mdat_customer.django_mdat_customer.admin
   :members:
   :undoc-members:
   :show-inheritance:

django\_mdat\_customer.django\_mdat\_customer.models module
-----------------------------------------------------------

.. automodule:: django_mdat_customer.django_mdat_customer.models
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_mdat_customer.django_mdat_customer
   :members:
   :undoc-members:
   :show-inheritance:
