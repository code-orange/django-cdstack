django\_cdstack\_tpl\_faxmail package
=====================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_faxmail.django_cdstack_tpl_faxmail

Submodules
----------

django\_cdstack\_tpl\_faxmail.setup module
------------------------------------------

.. automodule:: django_cdstack_tpl_faxmail.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_faxmail
   :members:
   :undoc-members:
   :show-inheritance:
