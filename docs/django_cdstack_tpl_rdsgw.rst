django\_cdstack\_tpl\_rdsgw package
===================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_rdsgw.django_cdstack_tpl_rdsgw

Submodules
----------

django\_cdstack\_tpl\_rdsgw.setup module
----------------------------------------

.. automodule:: django_cdstack_tpl_rdsgw.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_rdsgw
   :members:
   :undoc-members:
   :show-inheritance:
