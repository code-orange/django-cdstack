django\_cdstack\_tpl\_icinga2.django\_cdstack\_tpl\_icinga2 package
===================================================================

Submodules
----------

django\_cdstack\_tpl\_icinga2.django\_cdstack\_tpl\_icinga2.views module
------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_icinga2.django_cdstack_tpl_icinga2.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_icinga2.django_cdstack_tpl_icinga2
   :members:
   :undoc-members:
   :show-inheritance:
