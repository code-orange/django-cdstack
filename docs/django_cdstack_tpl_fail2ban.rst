django\_cdstack\_tpl\_fail2ban package
======================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban

Submodules
----------

django\_cdstack\_tpl\_fail2ban.setup module
-------------------------------------------

.. automodule:: django_cdstack_tpl_fail2ban.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_fail2ban
   :members:
   :undoc-members:
   :show-inheritance:
