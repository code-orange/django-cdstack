django\_cdstack\_metadata\_api package
======================================

Subpackages
-----------

.. toctree::

   django_cdstack_metadata_api.django_cdstack_metadata_api

Submodules
----------

django\_cdstack\_metadata\_api.setup module
-------------------------------------------

.. automodule:: django_cdstack_metadata_api.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_metadata_api
   :members:
   :undoc-members:
   :show-inheritance:
