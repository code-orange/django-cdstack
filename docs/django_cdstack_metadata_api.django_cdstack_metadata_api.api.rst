django\_cdstack\_metadata\_api.django\_cdstack\_metadata\_api.api package
=========================================================================

Submodules
----------

django\_cdstack\_metadata\_api.django\_cdstack\_metadata\_api.api.resources module
----------------------------------------------------------------------------------

.. automodule:: django_cdstack_metadata_api.django_cdstack_metadata_api.api.resources
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_metadata_api.django_cdstack_metadata_api.api
   :members:
   :undoc-members:
   :show-inheritance:
