django\_cdstack\_tpl\_cloudpbx package
======================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_cloudpbx.django_cdstack_tpl_cloudpbx

Submodules
----------

django\_cdstack\_tpl\_cloudpbx.setup module
-------------------------------------------

.. automodule:: django_cdstack_tpl_cloudpbx.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_cloudpbx
   :members:
   :undoc-members:
   :show-inheritance:
