django\_cdstack\_tpl\_thinclient.django\_cdstack\_tpl\_thinclient package
=========================================================================

Submodules
----------

django\_cdstack\_tpl\_thinclient.django\_cdstack\_tpl\_thinclient.views module
------------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_thinclient.django_cdstack_tpl_thinclient.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_thinclient.django_cdstack_tpl_thinclient
   :members:
   :undoc-members:
   :show-inheritance:
