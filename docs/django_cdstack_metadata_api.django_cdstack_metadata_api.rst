django\_cdstack\_metadata\_api.django\_cdstack\_metadata\_api package
=====================================================================

Subpackages
-----------

.. toctree::

   django_cdstack_metadata_api.django_cdstack_metadata_api.api

Submodules
----------

django\_cdstack\_metadata\_api.django\_cdstack\_metadata\_api.urls module
-------------------------------------------------------------------------

.. automodule:: django_cdstack_metadata_api.django_cdstack_metadata_api.urls
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_metadata_api.django_cdstack_metadata_api
   :members:
   :undoc-members:
   :show-inheritance:
