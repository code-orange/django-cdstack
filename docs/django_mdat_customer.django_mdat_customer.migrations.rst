django\_mdat\_customer.django\_mdat\_customer.migrations package
================================================================

Submodules
----------

django\_mdat\_customer.django\_mdat\_customer.migrations.0001\_initial module
-----------------------------------------------------------------------------

.. automodule:: django_mdat_customer.django_mdat_customer.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

django\_mdat\_customer.django\_mdat\_customer.migrations.0002\_add\_primary\_email\_and\_billing\_email module
--------------------------------------------------------------------------------------------------------------

.. automodule:: django_mdat_customer.django_mdat_customer.migrations.0002_add_primary_email_and_billing_email
   :members:
   :undoc-members:
   :show-inheritance:

django\_mdat\_customer.django\_mdat\_customer.migrations.0003\_add\_multitenancy\_user\_assignment module
---------------------------------------------------------------------------------------------------------

.. automodule:: django_mdat_customer.django_mdat_customer.migrations.0003_add_multitenancy_user_assignment
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_mdat_customer.django_mdat_customer.migrations
   :members:
   :undoc-members:
   :show-inheritance:
