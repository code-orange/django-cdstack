django\_cdstack\_tpl\_opennebula\_fe.django\_cdstack\_tpl\_opennebula\_fe package
=================================================================================

Submodules
----------

django\_cdstack\_tpl\_opennebula\_fe.django\_cdstack\_tpl\_opennebula\_fe.views module
--------------------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_opennebula_fe.django_cdstack_tpl_opennebula_fe.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_opennebula_fe.django_cdstack_tpl_opennebula_fe
   :members:
   :undoc-members:
   :show-inheritance:
