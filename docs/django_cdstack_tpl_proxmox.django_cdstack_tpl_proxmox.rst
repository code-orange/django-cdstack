django\_cdstack\_tpl\_proxmox.django\_cdstack\_tpl\_proxmox package
===================================================================

Submodules
----------

django\_cdstack\_tpl\_proxmox.django\_cdstack\_tpl\_proxmox.views module
------------------------------------------------------------------------

.. automodule:: django_cdstack_tpl_proxmox.django_cdstack_tpl_proxmox.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_proxmox.django_cdstack_tpl_proxmox
   :members:
   :undoc-members:
   :show-inheritance:
