django\_cdstack\_tpl\_phpworker package
=======================================

Subpackages
-----------

.. toctree::

   django_cdstack_tpl_phpworker.django_cdstack_tpl_phpworker

Submodules
----------

django\_cdstack\_tpl\_phpworker.setup module
--------------------------------------------

.. automodule:: django_cdstack_tpl_phpworker.setup
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: django_cdstack_tpl_phpworker
   :members:
   :undoc-members:
   :show-inheritance:
