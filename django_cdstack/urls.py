from django.contrib import admin

from django.conf import settings
from django.urls import include, path
from django.urls import re_path

urlpatterns = [
    path("files/", include("db_file_storage.urls")),
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path("deploy/", include("django_cdstack_deploy.django_cdstack_deploy.urls")),
    path(
        "api/metadata/",
        include("django_cdstack_metadata_api.django_cdstack_metadata_api.urls"),
    ),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
