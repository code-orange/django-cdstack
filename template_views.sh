#!/bin/bash
for d in django_cdstack_tpl_*/ ; do
	cd $d
	
	git checkout master
	
    echo "${PWD##*/}"
	cd "${PWD##*/}"
	
	current_path="${PWD##*/}"

	if [ ! -s ./views.py ] 
	then
		cp ../../buster_views.py views.py
		sed -i "s/XXXXXXXXXXXXXXXXXXXXXX/$current_path/g" views.py
		git add views.py
		git commit -m "add basic deployment view"
	fi
	
	#mkdir -p ./templates/config-fs/static/usr/sbin/

	#if [ ! -s ./templates/config-fs/static/usr/sbin/cmdb-deploy-specific-pre ] 
	#then
	#	cp ../../django_cdstack_tpl_deb_stretch/django_cdstack_tpl_deb_stretch/templates/config-fs/static/usr/sbin/cmdb-deploy-specific-pre ./templates/config-fs/static/usr/sbin/cmdb-deploy-specific-pre
	#	git add ./templates/config-fs/static/usr/sbin/cmdb-deploy-specific-pre
	#	git commit -m "add basic deployment script (pre)"
	#fi

	#if [ ! -s ./templates/config-fs/static/usr/sbin/cmdb-deploy-specific-post ] 
	#then
	#	cp ../../django_cdstack_tpl_deb_stretch/django_cdstack_tpl_deb_stretch/templates/config-fs/static/usr/sbin/cmdb-deploy-specific-post ./templates/config-fs/static/usr/sbin/cmdb-deploy-specific-post
	#	git add ./templates/config-fs/static/usr/sbin/cmdb-deploy-specific-post
	#	git commit -m "add basic deployment script (post)"
	#fi

	cd ..
	cd ..
done
