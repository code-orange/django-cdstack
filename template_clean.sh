#!/bin/bash
for d in django_cdstack_tpl_*/ ; do
	cd $d
	
	git checkout master
	
	git mv README.md README.rst
	git add README.rst
	git commit -m "rename README.rst"
		
	cp ../.gitignore .gitignore
	git add .gitignore
	git commit -m "add gitignore for python"
	
	cp ../LICENSE LICENSE
	git add LICENSE
	git commit -m "add LICENSE"
	
	touch "setup.py"
	git add setup.py
	git commit -m "add setup.py"
	
	touch "__init__.py"
	git add "__init__.py"
    echo "${PWD##*/}"
	mkdir "${PWD##*/}"
	cd "${PWD##*/}"
	touch "__init__.py"
	git add "__init__.py"
	cd ..
	git commit -m "init new module"

	cd "${PWD##*/}"
	touch "views.py"
	git add "views.py"
	cd ..
	git commit -m "add views.py"
	cd ..
done
